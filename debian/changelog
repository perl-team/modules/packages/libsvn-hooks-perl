libsvn-hooks-perl (1.36-1) unstable; urgency=medium

  * Import upstream version 1.36.

 -- gregor herrmann <gregoa@debian.org>  Sun, 11 Sep 2022 17:43:01 +0200

libsvn-hooks-perl (1.35-1) unstable; urgency=medium

  * Import upstream version 1.35.
  * Update debian/upstream/metadata.
  * debian/rules: remove override_dh_auto_install.
    Not needed since perl 5.26.
  * Update years of upstream and packaging copyright.
  * Drop fix-spelling-error-in-manpage.patch, applied upstream.
  * Update alternative (build) dependencies.
  * Declare compliance with Debian Policy 4.6.1.
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.
  * Remove debian/clean.
    The author test file is obsolete since many years.

 -- gregor herrmann <gregoa@debian.org>  Tue, 06 Sep 2022 22:45:32 +0200

libsvn-hooks-perl (1.34-3) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from deprecated 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Submit, Repository-Browse.
  * Remove obsolete field Name from debian/upstream/metadata (already present in
    machine-readable debian/copyright).

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 17 Jun 2022 11:11:04 +0100

libsvn-hooks-perl (1.34-2) unstable; urgency=medium

  * debian/rules: don't fail if README can't be removed.
    The newer ExtUtils::MakeMaker in perl 5.26 doesn't generate it anymore.
  * Update years of packaging copyright.
  * debian/copyright: use HTTPS for a URL. Thanks to duck.
  * Declare compliance with Debian Policy 4.0.0.

 -- gregor herrmann <gregoa@debian.org>  Sat, 24 Jun 2017 16:47:41 +0200

libsvn-hooks-perl (1.34-1) unstable; urgency=medium

  [ gregor herrmann ]
  * Remove Jonathan Yu from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Import upstream version 1.33
  * Drop (Build-)Depends(-Indep) on libjira-client-perl
  * Add (Build-)Depends(-Indep) on libjira-rest-perl
  * Drop spelling-error-in-manpage.patch patch
  * Import upstream version 1.34
  * Add debian/NEWS documenting major change for the CheckJira plugin
  * Add patch to fix spelling error in manpage

 -- Salvatore Bonaccorso <carnil@debian.org>  Thu, 10 Nov 2016 19:55:53 +0100

libsvn-hooks-perl (1.32-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * debian/upstream/metadata: use HTTPS for GitHub URLs.

  [ Salvatore Bonaccorso ]
  * Import upstream version 1.32
  * Update copyright years for upstream files
  * Update copyright years for debian/* packaging files
  * Make (Build-)Depends(-Indep) on libsvn-look-perl unversioned
  * Declare compliance with Debian policy 3.9.8
  * Add spelling-error-in-manpage.patch patch

 -- Salvatore Bonaccorso <carnil@debian.org>  Tue, 19 Jul 2016 20:12:42 +0200

libsvn-hooks-perl (1.31-1) unstable; urgency=medium

  * Import upstream version 1.31

 -- Salvatore Bonaccorso <carnil@debian.org>  Fri, 27 Nov 2015 06:19:47 +0100

libsvn-hooks-perl (1.30-1) unstable; urgency=medium

  * Import upstream version 1.30
  * Drop fix-spelling-error-in-manpage.patch patch

 -- Salvatore Bonaccorso <carnil@debian.org>  Sat, 29 Aug 2015 22:24:52 +0200

libsvn-hooks-perl (1.29-1) unstable; urgency=medium

  * Import upstream version 1.29
    + Fixes FTBFS due to failing tests in t/02-generic.t for pre-lock and
      pre-unlock hooks. Workaround bug in subversion 1.9 on the
      pre-lock/pre-unlock hooks that makes the commands lock/unlock succeed
      even if the hooks fail by skiping the two tests. (Closes: #795756)
  * Bump Debhelper compat level to 9
  * Add fix-spelling-error-in-manpage.patch patch

 -- Salvatore Bonaccorso <carnil@debian.org>  Mon, 24 Aug 2015 14:54:51 +0200

libsvn-hooks-perl (1.28-1) unstable; urgency=medium

  * Update Vcs-Browser URL to cgit web frontend
  * Add debian/upstream/metadata
  * Import upstream version 1.28
  * Update copyright years for upstream files
  * Update copyright years for debian/* packaging files
  * Declare compliance with Debian policy 3.9.6
  * Wrap and sort fields in debian/control file
  * Mark package as autopkgtestable
  * Don't install README.pod and generated manpage SVN::README.3pm.
    Add an override target for dh_auto_install to remove the installed
    README.pod and the generated corresponding manpage SVN::README.3pm.

 -- Salvatore Bonaccorso <carnil@debian.org>  Sun, 31 May 2015 16:25:48 +0200

libsvn-hooks-perl (1.27-1) unstable; urgency=medium

  * Imported Upstream version 1.27

 -- Angel Abad <angel@debian.org>  Sun, 10 Aug 2014 10:18:20 +0200

libsvn-hooks-perl (1.26-1) unstable; urgency=medium

  * Imported Upstream version 1.26
  * debian/copyright: Update copyright years.

 -- Angel Abad <angel@debian.org>  Fri, 04 Jul 2014 17:58:33 +0200

libsvn-hooks-perl (1.25-1) unstable; urgency=medium

  * Imported Upstream version 1.25

 -- Salvatore Bonaccorso <carnil@debian.org>  Mon, 21 Apr 2014 21:30:17 +0200

libsvn-hooks-perl (1.24-1) unstable; urgency=medium

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Imported Upstream version 1.24
  * Update copyright years for upstream files
  * Update copyright years for debian/* packaging files

 -- Salvatore Bonaccorso <carnil@debian.org>  Sat, 22 Mar 2014 22:56:17 +0100

libsvn-hooks-perl (1.23-2) unstable; urgency=medium

  * Update (build) dependencies. XMLRPC::Lite is now in its own package.
    Thanks to David Suárez for the bug report. (Closes: #738419)
  * Update years of packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Sun, 09 Feb 2014 19:10:51 +0100

libsvn-hooks-perl (1.23-1) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Downgrade debhelper dependency to compat level 8

  [ Angel Abad ]
  * Imported Upstream version 1.23
  * Bump Standards-Version to 3.9.5 (no changes)

 -- Angel Abad <angel@debian.org>  Sun, 24 Nov 2013 11:44:38 +0100

libsvn-hooks-perl (1.22-1) unstable; urgency=low

  * Imported Upstream version 1.22
  * Use debhelper 9.20120312 to get all hardening flags.
  * debian/copyright: Update debian/* years

 -- Angel Abad <angel@debian.org>  Wed, 20 Nov 2013 13:23:37 +0100

libsvn-hooks-perl (1.21-1) unstable; urgency=low

  * Imported Upstream version 1.21

 -- Salvatore Bonaccorso <carnil@debian.org>  Tue, 18 Jun 2013 22:40:22 +0200

libsvn-hooks-perl (1.20-1) unstable; urgency=low

  * Imported Upstream version 1.20
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs
  * Update copyright years.
    Update copyright years for upstream files.
    Update copyright years for debian/* packaging files.
  * Bump Standards-Version to 3.9.4

 -- Salvatore Bonaccorso <carnil@debian.org>  Thu, 13 Jun 2013 21:41:00 +0200

libsvn-hooks-perl (1.19-1) unstable; urgency=low

  * Imported Upstream version 1.19

 -- Salvatore Bonaccorso <carnil@debian.org>  Mon, 25 Jun 2012 13:07:50 +0200

libsvn-hooks-perl (1.18-1) unstable; urgency=low

  [ Angel Abad ]
  * Imported Upstream version 1.16

  [ gregor herrmann ]
  * New upstream release 1.18.
  * Add (build) dependency on libdata-util-perl.
  * Update years of packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Sun, 27 May 2012 14:43:40 +0200

libsvn-hooks-perl (1.15-1) unstable; urgency=low

  * Imported Upstream version 1.15

 -- Salvatore Bonaccorso <carnil@debian.org>  Sat, 21 Apr 2012 10:43:02 +0200

libsvn-hooks-perl (1.13-1) unstable; urgency=low

  * Imported Upstream version 1.13

 -- Angel Abad <angel@debian.org>  Wed, 07 Mar 2012 09:34:21 +0100

libsvn-hooks-perl (1.12-1) unstable; urgency=low

  * Email change: Angel Abad -> angel@debian.org
  * Imported Upstream version 1.12
  * debian/copyright:
    - Update format as in Debian Policy 3.9.3
    - Update copyright years
    - Fix typo in license name
  * Bump Standards-Version to 3.9.3

 -- Angel Abad <angel@debian.org>  Sun, 26 Feb 2012 14:32:11 +0100

libsvn-hooks-perl (1.11-1) unstable; urgency=low

  [ Angel Abad ]
  * Imported Upstream version 1.11

  [ gregor herrmann ]
  * Install the new examples (with the x bit stripped, they are just
    snippets).

 -- Angel Abad <angelabad@gmail.com>  Mon, 05 Dec 2011 09:55:16 +0100

libsvn-hooks-perl (1.10-1) unstable; urgency=low

  * Imported Upstream version 1.10

 -- Angel Abad <angelabad@gmail.com>  Wed, 09 Nov 2011 18:57:21 +0100

libsvn-hooks-perl (1.09-1) unstable; urgency=low

  * Imported Upstream version 1.09

 -- Salvatore Bonaccorso <carnil@debian.org>  Thu, 03 Nov 2011 08:19:42 +0100

libsvn-hooks-perl (1.08-1) unstable; urgency=low

  * Imported Upstream version 1.08
  * Drop unused (Build-)Depends(-Indep).
    Upstream deprecated SVN::Hooks::Mailer. This allow dropping
    (Build-)Depends(-Indep) on libemail-send-perl and libemail-simple-perl
    (>= 2.100) | libemail-simple-creator-perl. (Closes: #639880)

 -- Salvatore Bonaccorso <carnil@debian.org>  Mon, 26 Sep 2011 20:38:26 +0200

libsvn-hooks-perl (1.07-1) unstable; urgency=low

  * Imported Upstream version 1.07

 -- Angel Abad <angelabad@gmail.com>  Sun, 28 Aug 2011 10:13:37 +0200

libsvn-hooks-perl (1.05-1) unstable; urgency=low

  * Imported Upstream version 1.05
  * debian/copyright: Update copyright years for debian/*
    packaging stanza.

 -- Salvatore Bonaccorso <carnil@debian.org>  Sat, 13 Aug 2011 10:10:17 +0200

libsvn-hooks-perl (1.04-1) unstable; urgency=low

  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.
  * Imported Upstream version 1.04

 -- Salvatore Bonaccorso <carnil@debian.org>  Thu, 11 Aug 2011 22:05:59 +0200

libsvn-hooks-perl (1.03-1) unstable; urgency=low

  * New uptream release

 -- Angel Abad <angelabad@gmail.com>  Sun, 31 Jul 2011 12:37:25 +0200

libsvn-hooks-perl (1.02-1) unstable; urgency=low

  [ Angel Abad ]
  * New upstream release
  * debian/control:
    - Build-Depends on liburi-perl
    - Bump minimum libsvn-look-perl version to 0.21

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

 -- Angel Abad <angelabad@gmail.com>  Thu, 28 Jul 2011 10:54:12 +0200

libsvn-hooks-perl (1.01-1) unstable; urgency=low

  * New upstream release
  * debian/control:
    - Remove libfile-slurp-perl from Build-Depends, now isnt necessary
  * debian/copyright:
    - Update copyright years

 -- Angel Abad <angelabad@gmail.com>  Tue, 26 Jul 2011 00:20:17 +0200

libsvn-hooks-perl (1.00-1) unstable; urgency=low

  * New upstream release
  * debian/control:
    - Build-Depends on libfile-slurp-perl to fix tests FTBFS

 -- Angel Abad <angelabad@gmail.com>  Thu, 21 Jul 2011 15:26:50 +0200

libsvn-hooks-perl (0.91-1) unstable; urgency=low

  * New upstream release
  * Bump to debhelper compat 8

 -- Angel Abad <angelabad@gmail.com>  Fri, 08 Jul 2011 14:56:33 +0200

libsvn-hooks-perl (0.90-1) unstable; urgency=low

  * New upstream release
  * debian/patches/pod.patch: Removed, fixed upstream
  * Bump Standards-Version to 3.9.2 (no changes)

 -- Angel Abad <angelabad@gmail.com>  Tue, 17 May 2011 13:37:05 +0200

libsvn-hooks-perl (0.33-1) unstable; urgency=low

  * New upstream release
  * Refresh debian/copyright: Update copyright years for debian/* packaging.

 -- Angel Abad <angelabad@gmail.com>  Mon, 17 Jan 2011 03:37:04 +0100

libsvn-hooks-perl (0.32-1) unstable; urgency=low

  * New upstream release

 -- Angel Abad <angelabad@gmail.com>  Sun, 12 Dec 2010 16:10:04 +0100

libsvn-hooks-perl (0.31-1) unstable; urgency=low

  * New upstream release
  * Update my email address.
  * debian/copyright: Refer to Debian systems in general instead of only
    Debian GNU/Linux systems.

 -- Salvatore Bonaccorso <carnil@debian.org>  Sat, 11 Dec 2010 08:19:11 +0100

libsvn-hooks-perl (0.30-1) unstable; urgency=low

  * New upstream release
  * debian/copyright: Update license information
  * debian/patches/pod.patch: Update patch
  * Bump Standards-Version to 3.9.1 (no changes)

 -- Angel Abad <angelabad@gmail.com>  Fri, 08 Oct 2010 00:53:57 +0200

libsvn-hooks-perl (0.28-1) unstable; urgency=low

  * New upstream release
  * Update copyright years for debian/* packaging.

 -- Salvatore Bonaccorso <salvatore.bonaccorso@gmail.com>  Thu, 29 Apr 2010 14:22:57 +0200

libsvn-hooks-perl (0.27-1) unstable; urgency=low

  * New upstream release
  * Standards-Version 3.8.4 (no changes)
  * Add myself to Uploaders and Copyright
  * Rewrite control description
  * Use new 3.0 (quilt) source format
  * No longer run author tests (Kwalitee and PerlCritic are prone to
    failure; Pod Coverage fails)

 -- Jonathan Yu <jawnsy@cpan.org>  Thu, 11 Mar 2010 20:30:22 +0100

libsvn-hooks-perl (0.25-1) unstable; urgency=low

  * New upstream release
  * debian/control:
    - Remove perl from Depends

 -- Angel Abad <angelabad@gmail.com>  Wed, 20 Jan 2010 21:14:08 +0100

libsvn-hooks-perl (0.24-1) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: replace (build) dependency on libemail-simple-
    creator-perl with "libemail-simple-perl (>= 2.100) | libemail-
    simple-creator-perl".

  [ Angel Abad ]
  * New upstream release
  * Refresh quilt patch

 -- Angel Abad <angelabad@gmail.com>  Fri, 08 Jan 2010 15:01:42 +0100

libsvn-hooks-perl (0.23-1) unstable; urgency=low

  * Update my email address
  * New upstream release
  * debian/rules: Now use --with quilt
  * Add libtest-kwalitee-perl in B-D-I

 -- Angel Abad <angelabad@gmail.com>  Sun, 25 Oct 2009 08:22:51 +0100

libsvn-hooks-perl (0.22-1) unstable; urgency=low

  * New upstream release

 -- Angel Abad (Ikusnet SLL) <angel@grupoikusnet.com>  Thu, 24 Sep 2009 10:34:11 +0200

libsvn-hooks-perl (0.21-2) unstable; urgency=low

  * Add a patch to make the POD work with Pod::Simple >= 3.08; add quilt
    framework (closes: #546623).

 -- gregor herrmann <gregoa@debian.org>  Sat, 19 Sep 2009 22:46:04 +0200

libsvn-hooks-perl (0.21-1) unstable; urgency=low

  [Salvatore Bonaccorso]
  * debian/control: Changed: Replace versioned (build-)dependency on
    perl (>= 5.6.0-{12,16}) with an unversioned dependency on perl (as
    permitted by Debian Policy 3.8.3).

  [ Angel Abad (Ikusnet SLL) ]
  * New upstream release
  * debian/control:
    - Bump Standards-Version to 3.8.3

  [ gregor herrmann ]
  * Enable author-tests (debian/{rules,control,clean}).

 -- Angel Abad (Ikusnet SLL) <angel@grupoikusnet.com>  Thu, 20 Aug 2009 12:46:32 +0200

libsvn-hooks-perl (0.20-1) unstable; urgency=low

  * New upstream release

 -- Salvatore Bonaccorso <salvatore.bonaccorso@gmail.com>  Wed, 29 Jul 2009 08:59:26 +0200

libsvn-hooks-perl (0.18-1) unstable; urgency=low

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Salvatore Bonaccorso ]
  * New upstream release
  * debian/control
    - Bump Standards-Version to 3.8.2 (no changes)
    - Add myself to Uploaders
  * Update copyright information for debian/* packaging
  * debian/rules: Simplify rules makefile
  * debian/watch: Remove comments

 -- Salvatore Bonaccorso <salvatore.bonaccorso@gmail.com>  Sat, 25 Jul 2009 18:23:02 +0200

libsvn-hooks-perl (0.17.54-1) unstable; urgency=low

  * New upstream release.
  * debian/copyright: update years of upstream copyright, add /me to copyright
    for debian/*.
  * Add /me to Uploaders.
  * debian/rules: add the new --noonline-tests option to dh_auto_configure to
    make sure we don't run tests that require network access; bump debhelper
    dependency in debian/control.

 -- gregor herrmann <gregoa@debian.org>  Tue, 28 Apr 2009 17:58:03 +0200

libsvn-hooks-perl (0.16.52-1) unstable; urgency=low

  * New upstream release

 -- Angel Abad (Ikusnet SLL) <angel@grupoikusnet.com>  Mon, 13 Apr 2009 14:56:39 +0200

libsvn-hooks-perl (0.15.41-1) unstable; urgency=low

  [ Angel Abad (Ikusnet SLL) ]
  * New upstream release

  [ gregor herrmann ]
  * Set Standards-Version to 3.8.1 (no changes).

 -- Angel Abad (Ikusnet SLL) <angel@grupoikusnet.com>  Fri, 13 Mar 2009 10:24:16 +0100

libsvn-hooks-perl (0.14.38-1) unstable; urgency=low

  * New upstream release

 -- Angel Abad (Ikusnet SLL) <angel@grupoikusnet.com>  Sun, 08 Mar 2009 14:54:04 +0100

libsvn-hooks-perl (0.13.12-1) unstable; urgency=low

  * Initial Release. (Closes: #507268)

 -- Angel Abad (Ikusnet SLL) <angel@grupoikusnet.com>  Thu, 22 Jan 2009 10:37:24 +0100
